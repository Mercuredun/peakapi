<?php 
namespace PeakAPI\PEAK;
use \Exception;

class Expenses
{
    protected $getExpensesUrl = '/expenses';

    public function __construct()
    {}

    public function postExpenses($token, $body = [])
    {
        $postBody = [
            'PeakExpenses' => [
                'expenses' => $body
            ]
        ];

        $url = sprintf('%s%s', peakConfig::$fullUrl, $this->getExpensesUrl);

        try {
            $returnExpenses = Util::postApi($url, $postBody, $token);

            if (empty($returnExpenses)) {
                throw new Exception('Return Null !!');
            } else {
                $jsonstr = json_decode($returnExpenses['body'], true);

                if ($jsonstr['PeakExpenses']['resCode'] == '200') {
                    return $jsonstr['PeakExpenses'];
                } else {
                    throw new Exception($jsonstr['PeakExpenses']['resDesc']);
                }
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getExpenses($token, $code = '')
    {
        $url = sprintf('%s%s', peakConfig::$fullUrl, $this->getExpensesUrl);
        $url = ($code != '') ? sprintf('%s?code=%s', $url, $code) : $url;

        try {
            $returnExpenses = Util::getApi($url, $token);

            if (empty($returnExpenses)) {
                throw new Exception('Return Null !!');
            } else {
                $jsonstr = json_decode($returnExpenses['body'], true);

                if ($jsonstr['PeakExpenses']['resCode'] == '200') {
                    return $jsonstr['PeakExpenses']['expenses'];
                } else {
                    throw new Exception($jsonstr['PeakExpenses']['resDesc']);
                }
            }
        } catch (Exception $e) {
            throw $e;
        }
    }
}
