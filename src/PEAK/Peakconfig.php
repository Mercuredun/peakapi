<?php 
namespace PeakAPI\PEAK;

class PeakConfig
{
    static $baseUrl = 'api';
    static $apiVersion = 'v1';
    static $fullUrl = '';
    static $productionUrl = 'http://peakengineapi.azurewebsites.net';
    static $uatUrl = 'http://peakengineapidev.azurewebsites.net';
    static $connectId = '';

    public static function getConfig($id, $mode = 'test')
    {
        $host = ($mode == 'test') ? self::$uatUrl : self::$productionUrl;
        $url = sprintf('%s/%s/%s', $host, self::$baseUrl, self::$apiVersion);
        self::$fullUrl = $url;
        self::$connectId = $id;

        return $url;
    }
}
