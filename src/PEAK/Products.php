<?php 
namespace PeakAPI\PEAK;
use \Exception;

class Products
{
    protected $getProductsUrl = '/products';

    public function __construct()
    {}

    public function postProducts($token, $body = [])
    {
        $postBody = [
            'PeakProducts' => [
                'products' => $body
            ]
        ];

        $url = sprintf('%s%s', peakConfig::$fullUrl, $this->getProductsUrl);

        try {
            $returnProducts = Util::postApi($url, $postBody, $token);

            if (empty($returnProducts)) {
                throw new Exception('Return Null !!');
            } else {
                $jsonstr = json_decode($returnProducts['body'], true);

                if ($jsonstr['PeakProducts']['resCode'] == '200') {
                    return $jsonstr['PeakProducts'];
                } else {
                    throw new Exception($jsonstr['PeakProducts']['resDesc']);
                }
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getProducts($token, $code = '')
    {
        $url = sprintf('%s%s', peakConfig::$fullUrl, $this->getProductsUrl);
        $url = ($code != '') ? sprintf('%s?code=%s', $url, $code) : $url;

        try {
            $returnProducts = Util::getApi($url, $token);

            if (empty($returnProducts)) {
                throw new Exception('Return Null !!');
            } else {
                $jsonstr = json_decode($returnProducts['body'], true);

                if ($jsonstr['PeakProducts']['resCode'] == '200') {
                    return $jsonstr['PeakProducts']['products'];
                } else {
                    throw new Exception($jsonstr['PeakProducts']['resDesc']);
                }
            }
        } catch (Exception $e) {
            throw $e;
        }
    }
}
