<?php 
namespace PeakAPI\PEAK;
use \Exception;

class Contacts
{
    protected $getContactsUrl = '/contacts';
    protected $getEditContactsUrl = '/contacts/edit';

    public function __construct()
    {}

    public function postContacts($token, $body = [])
    {
        $postBody = [
            'PeakContacts' => [
                'contacts' => $body
            ]
        ];

        $url = sprintf('%s%s', peakConfig::$fullUrl, $this->getContactsUrl);

        try {
            $returnContacts = Util::postApi($url, $postBody, $token);

            if (empty($returnContacts)) {
                throw new Exception('Return Null !!');
            } else {
                $jsonstr = json_decode($returnContacts['body'], true);

                if ($jsonstr['PeakContacts']['resCode'] == '200') {
                    return $jsonstr['PeakContacts'];
                } else {
                    throw new Exception($jsonstr['PeakContacts']['resDesc']);
                }
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getContacts($token, $code = '')
    {
        $url = sprintf('%s%s', peakConfig::$fullUrl, $this->getContactsUrl);
        $url = ($code != '') ? sprintf('%s?code=%s', $url, $code) : $url;

        try {
            $returnContacts = Util::getApi($url, $token);

            if (empty($returnContacts)) {
                throw new Exception('Return Null !!');
            } else {
                $jsonstr = json_decode($returnContacts['body'], true);

                if ($jsonstr['PeakContacts']['resCode'] == '200') {
                    return $jsonstr['PeakContacts']['contacts'];
                } else {
                    throw new Exception($jsonstr['PeakContacts']['resDesc']);
                }
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function editContacts($token, $id, $body = [])
    {
        $postBody = [
            'PeakContacts' => [
                'id' => $id,
                'contacts' => $body
            ]
        ];

        $url = sprintf('%s%s', peakConfig::$fullUrl, $this->getEditContactsUrl);

        try {
            $returnContacts = Util::postApi($url, $postBody, $token);

            if (empty($returnContacts)) {
                throw new Exception('Return Null !!');
            } else {
                $jsonstr = json_decode($returnContacts['body'], true);

                if ($jsonstr['PeakContacts']['resCode'] == '200') {
                    return $jsonstr['PeakContacts'];
                } else {
                    throw new Exception($jsonstr['PeakContacts']['resDesc']);
                }
            }
        } catch (Exception $e) {
            throw $e;
        }
    }
}
