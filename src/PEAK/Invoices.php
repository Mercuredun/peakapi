<?php 
namespace PeakAPI\PEAK;
use \Exception;

class Invoices
{
    protected $getInvoicesUrl = '/invoices';

    public function __construct()
    {}

    public function postInvoices($token, $body = [])
    {
        if ($body['PeakInvoices']['invoices']['contactId'] && $body['PeakInvoices']['invoices']['contactCode']) {
            throw new Exception('Just use only contactId or contactCode.');
        }

        $postBody = [
            'PeakInvoices' => [
                'invoices' => $body
            ]
        ];

        $url = sprintf('%s%s', peakConfig::$fullUrl, $this->getInvoicesUrl);

        try {
            $returnInvoices = Util::postApi($url, $postBody, $token);

            if (empty($returnInvoices)) {
                throw new Exception('Return Null !!');
            } else {
                $jsonstr = json_decode($returnInvoices['body'], true);

                if ($jsonstr['PeakInvoices']['resCode'] == '200') {
                    return $jsonstr['PeakInvoices'];
                } else {
                    throw new Exception($jsonstr['PeakInvoices']['resDesc']);
                }
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getInvoices($token, $code = '')
    {
        $url = sprintf('%s%s', peakConfig::$fullUrl, $this->getInvoicesUrl);
        $url = ($code != '') ? sprintf('%s?code=%s', $url, $code) : $url;

        try {
            $returnInvoices = Util::getApi($url, $token);

            if (empty($returnInvoices)) {
                throw new Exception('Return Null !!');
            } else {
                $jsonstr = json_decode($returnInvoices['body'], true);

                if ($jsonstr['PeakInvoices']['resCode'] == '200') {
                    return $jsonstr['PeakInvoices']['invoices'];
                } else {
                    throw new Exception($jsonstr['PeakInvoices']['resDesc']);
                }
            }
        } catch (Exception $e) {
            throw $e;
        }
    }
}
