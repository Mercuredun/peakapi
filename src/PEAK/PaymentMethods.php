<?php 
namespace PeakAPI\PEAK;
use \Exception;

class PaymentMethods
{
    protected $getPMUrl = '/paymentmethods';

    public function __construct()
    {}

    public function postPaymentMethods($token, $body = [])
    {
        $postBody = [
            'PeakPaymentMethods' => [
                'paymentMethods' => $body
            ]
        ];

        $url = sprintf('%s%s', peakConfig::$fullUrl, $this->getPMUrl);

        try {
            $returnPM = Util::postApi($url, $postBody, $token);

            if (empty($returnPM)) {
                throw new Exception('Return Null !!');
            } else {
                $jsonstr = json_decode($returnPM['body'], true);

                if ($jsonstr['PeakPaymentMethods']['resCode'] == '200') {
                    return $jsonstr['PeakPaymentMethods'];
                } else {
                    throw new Exception($jsonstr['PeakPaymentMethods']['resDesc']);
                }
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getPaymentMethods($token, $code = '')
    {
        $url = sprintf('%s%s', peakConfig::$fullUrl, $this->getPMUrl);
        $url = ($code != '') ? sprintf('%s?code=%s', $url, $code) : $url;

        try {
            $returnPM = Util::getApi($url, $token);

            if (empty($returnPM)) {
                throw new Exception('Return Null !!');
            } else {
                $jsonstr = json_decode($returnPM['body'], true);

                if ($jsonstr['PeakPaymentMethods']['resCode'] == '200') {
                    return $jsonstr['PeakPaymentMethods']['paymentMethods'];
                } else {
                    throw new Exception($jsonstr['PeakPaymentMethods']['resDesc']);
                }
            }
        } catch (Exception $e) {
            throw $e;
        }
    }
}
