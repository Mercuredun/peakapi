<?php 
namespace PeakAPI\PEAK;
use \Exception;

class Receipts
{
    protected $getReceiptsUrl = '/receipts';

    public function __construct()
    {}

    public function postReceipts($token, $body = [])
    {
        $postBody = [
            'PeakReceipts' => [
                'receipts' => $body
            ]
        ];

        $url = sprintf('%s%s', peakConfig::$fullUrl, $this->getReceiptsUrl);

        try {
            $returnReceipts = Util::postApi($url, $postBody, $token);

            if (empty($returnReceipts)) {
                throw new Exception('Return Null !!');
            } else {
                $jsonstr = json_decode($returnReceipts['body'], true);

                if ($jsonstr['PeakReceipts']['resCode'] == '200') {
                    return $jsonstr['PeakReceipts'];
                } else {
                    throw new Exception($jsonstr['PeakReceipts']['resDesc']);
                }
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getReceipts($token, $code = '')
    {
        $url = sprintf('%s%s', peakConfig::$fullUrl, $this->getReceiptsUrl);
        $url = ($code != '') ? sprintf('%s?code=%s', $url, $code) : $url;

        try {
            $returnReceipts = Util::getApi($url, $token);

            if (empty($returnReceipts)) {
                throw new Exception('Return Null !!');
            } else {
                $jsonstr = json_decode($returnReceipts['body'], true);

                if ($jsonstr['PeakReceipts']['resCode'] == '200') {
                    return $jsonstr['PeakReceipts']['receipts'];
                } else {
                    throw new Exception($jsonstr['PeakReceipts']['resDesc']);
                }
            }
        } catch (Exception $e) {
            throw $e;
        }
    }
}

