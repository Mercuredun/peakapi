<?php 
namespace PeakAPI\PEAK;

use \Datetime;
use \DatetimeZone;
use \Exception;

class Util 
{
    public function __construct()
    {}

    public static function getTimeStamp($str_server_timezone = 'UTC',
       $str_server_dateformat = 'YmdHis') 
    {
 
        $date = new DateTime('now');
        $date->setTimezone(new DateTimeZone($str_server_timezone));
        $str_server_now = $date->format($str_server_dateformat);
 
        date_default_timezone_set($str_server_timezone);
 
        return $str_server_now;
    }

    public static function getTimeSignature($timeStamp)
    {
        return hash_hmac('sha1', $timeStamp, peakConfig::$connectId);
    }

    public static function getHeader($token = false)
    {
        $timeStamp = self::getTimeStamp();
        $timeSignature = self::getTimeSignature($timeStamp);

        $headers = [
            'Content-Type:application/json',
            'Time-Stamp:' . $timeStamp,
            'Time-Signature:' . $timeSignature,
            'Client-Token:' . $token->getClientToken(),
            'User-Token:' . $token->getUserToken(),
        ];

        return $headers;
    }

    public static function postApi($url, $body = [], $token)
    {
        $postValue = json_encode($body);
        $header = self::getHeader($token);

        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $postValue,
            CURLOPT_HTTPHEADER => $header
        ]);

        $response = curl_exec($curl);
        $info = curl_getinfo($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            throw new Exception('cURL Error #:' . $err);
        } else {
            return ['header' => $info, 'body' => $response];
        }
    }

    public static function getApi($url, $token)
    {
        $header = self::getHeader($token);
        
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => $header
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            throw new Exception('cURL Error #:' . $err);
        } else {
            return ['header' => $info, 'body' => $response];
        }
    }
}
