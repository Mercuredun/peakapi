<?php 
namespace PeakAPI\PEAK;
use \Exception;

class Token
{
    protected $getTokenUrl = '/clienttoken';
    protected $clientToken = '';
    protected $userToken = '';

    public function __construct($clientToken = '', $userToken = '')
    {
        $this->clientToken = ($clientToken != '') ? $clientToken : '';
        $this->userToken = ($userToken != '') ? $userToken : '';
    }

    public function setClientToken($connectId = '', $password = '')
    {
        $timeStamp = Util::getTimeStamp();
        $timeSignature = Util::getTimeSignature($timeStamp);

        $header = [
            'Content-Type:application/json',
            'Time-Stamp:' . $timeStamp,
            'Time-Signature:' . $timeSignature
        ];

        $body = [
            'PeakClientToken' => [
                'connectId' => $connectId,
                'password' => $password
            ]
        ];

        $url = sprintf('%s%s', peakConfig::$fullUrl, $this->getTokenUrl);
        $postValue = json_encode($body);

        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $postValue,
            CURLOPT_HTTPHEADER => $header
        ]);

        $response = curl_exec($curl);
        $info = curl_getinfo($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            throw new Exception('cURL Error #:' . $err);
        } else {
            $returnToken = ['header' => $info, 'body' => $response];
        }

        if (empty($returnToken)) {
            throw new Exception('Return Null !!');
        } else {
            $jsonstr = json_decode($returnToken['body'], true);
            
            if ($jsonstr['PeakClientToken']['resCode'] == '200') {
                $this->clientToken = $jsonstr['PeakClientToken']['token'];
                return true;
            } else {
                throw new Exception($jsonstr['PeakClientToken']['resDesc']);
            }
        }
    }

    public function setUserToken($userToken = '')
    {
        $this->userToken = $userToken;
    }

    public function getClientToken()
    {
        return $this->clientToken;
    }

    public function getUserToken()
    {
        return $this->userToken;
    }
}
